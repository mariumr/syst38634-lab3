import java.util.Scanner;

public class Code {

	public static void countUnicode(String string1) {

		int codepoint = string1.codePointCount(1,15);
		System.out.println("Codepoint count = " + codepoint);

	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter a first string to count number of Unicode code points \n");
		String myString1 = scan.nextLine();
		countUnicode(myString1);

	}

}
