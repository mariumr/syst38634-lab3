import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import Time.TimeTest;

@RunWith(Suite.class)
@SuiteClasses({ TimeTest.class })
public class AllTests {

}
