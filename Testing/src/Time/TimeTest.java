package Time;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class TimeTest {

	private String timeToTest;
	
	
	
	public TimeTest(String dataTime) {
		timeToTest = dataTime;
	}
	
	@Test
	public  void testGetMilliseconds() {
		int milli = Time.getMilliSeconds("12:05:05:05") ;
		System.out.println(milli);
		assertTrue("The milliseconds were not calculated properly" , milli ==5);
	}
	
	@Test (expected = NumberFormatException.class)
	public  void testGetMillisecondsException() {
		int milli = Time.getMilliSeconds("12:05:05:5405") ;
		System.out.println(milli);
		assertTrue("The milliseconds were not calculated properly" , milli == 5);
	}
	
	@Test
	public void testGetMillisecondsBoundryIn() {
		int milli = Time.getMilliSeconds("12:05:05:999") ;
		System.out.println(milli);
		assertTrue("The milliseconds were not calculated properly" , milli == 999);
	}
	
	@Test(expected = NumberFormatException.class)
	public  void testGetMillisecondsBoundryOut() {
		int milli = Time.getMilliSeconds("12:05:05:1000") ;
		System.out.println(milli);
		assertTrue("The milliseconds were not calculated properly" , milli == 0);
	}
	
	@Parameterized.Parameters
	public static Collection<Object [ ]> loadData( ) {

	Object [ ][ ] data = { { "12:05:05" } };
	return Arrays.asList( data );
	}
	
	/*
	@Test
	public void testGetTotalSeconds() {
		int seconds = Time.getTotalSeconds("12:05:05");
		assertTrue("The seconds were not calculated properly", seconds == 43505);
	}
*/
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
    /*@Test
	public void testGetSeconds() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalMinutes() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalHours() {
		fail("Not yet implemented");
	}
*/
}
