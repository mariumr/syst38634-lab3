import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 */

/**
 * @author HP
 *
 */
public class PasswordValidatorTest {
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
    public String passwordToTest;
	
	public PasswordValidatorTest() {}

	/**
	 * Test method for {@link PasswordValidator#checkPasswordlength(java.lang.String)}.
	 */
	@Test
	public void testCheckPasswordlength() {
	       boolean isPasswordValid = PasswordValidator.checkPasswordlength("o7yt5r4re");
	       assertTrue("The password must be atleast 8 characters" , isPasswordValid==false);
		}
	@Test
	public void testCheckPasswordlengthException() {
	       boolean isPasswordValid = PasswordValidator.checkPasswordlength("h65k");
	       assertTrue("The password must be atleast 8 characters" , isPasswordValid==true);
		}
	@Test
	public void testCheckPasswordlengthBoundryIn() {
	       boolean isPasswordValid = PasswordValidator.checkPasswordlength("9k8j7h6g");
	       assertTrue("The password must be atleast 8 characters" , isPasswordValid==false);
		}
	@Test
	public void testCheckPasswordlengthBoundryOut() {
	       boolean isPasswordValid = PasswordValidator.checkPasswordlength("g5sf6vm");
	       assertTrue("The password must be atleast 8 characters" , isPasswordValid==true);
		}
	@Test
	public void testCheckDigits() {
		boolean password = PasswordValidator.checkDigits("g6h7d5e5");
		System.out.println(password);
	    assertTrue("The password should contain atleast 2 digits", password == false);
	}
	@Test
	public void testCheckDigitsException() {
		boolean password = PasswordValidator.checkDigits("kzmshs");
		System.out.println(password);
	    assertTrue("The password should contain atleast 2 digits", password == true);
	}
	@Test
	public void testCheckDigitsBoundryIn() {
		boolean password = PasswordValidator.checkDigits("p4k4bd");
		System.out.println(password);
	    assertTrue("The password should contain atleast 2 digits", password == false);
	}
	@Test
	public void testCheckDigitsBoundryOut() {
		boolean password = PasswordValidator.checkDigits("hhh5hhh");
		System.out.println(password);
	    assertTrue("The password should contain atleast 2 digits", password == true);
	}

}
